// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event HasVoted (address voter, uint proposalId);

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    // This declares a new complex type which will
    // be used for variables later.
    // It will represent a single voter.
    struct Voter {
        bool isRegistered;
        uint weight;
        bool hasVoted;  // if true, that person already hasVoted
        uint vote;   // index of the hasVoted proposal
    }

    // This is a type for a single proposal.
    struct Proposal {
        string name;   // short name (up to 32 bytes)
        uint voteCount; // number of accumulated votes
    }

    address public chairperson;

    // This declares a state variable that
    // stores a `Voter` struct for each possible address.
    mapping(address => Voter) public voters;

    uint numberOfVoters = 0;
    uint numberOfParticipants = 0;

    // A dynamically-sized array of `Proposal` structs.
    Proposal[] public proposals;

    uint winningProposalId;

    // The current step in the vote process.
    WorkflowStatus step;


    /// Create a new ballot to choose one of `proposalNames`.
    constructor(uint chairpersonWeight) Ownable(msg.sender) {
        chairperson = msg.sender;
        voters[chairperson].isRegistered = true;
        voters[chairperson].weight = chairpersonWeight;
        step = WorkflowStatus.RegisteringVoters;
        numberOfParticipants++;
    }

    function getProposals() public view
            returns (string[] memory titles) {

        titles = new string[](proposals.length);
        for (uint256 i = 0; i < proposals.length; i++) {
            titles[i] = proposals[i].name;
        }
    }


    function changeStep(uint newStep) external onlyOwner {
        require(
            uint(WorkflowStatus.RegisteringVoters) <= newStep && newStep <= uint(WorkflowStatus.VotesTallied), 
            "Unknown step."
        );
        WorkflowStatus previousStep = step;
        step = WorkflowStatus(newStep);
        emit WorkflowStatusChange(previousStep, step);
    }

    // Give `voter` the right to vote on this ballot.
    // May only be called by `chairperson`.
    function giveRightToVote(address voter, uint weight) external onlyOwner {
        // If the first argument of `require` evaluates
        // to `false`, execution terminates and all
        // changes to the state and to Ether balances
        // are reverted.
        // This used to consume all gas in old EVM versions, but
        // not anymore.
        // It is often a good idea to use `require` to check if
        // functions are called correctly.
        // As a second argument, you can also provide an
        // explanation about what went wrong.
        require(step == WorkflowStatus.RegisteringVoters, "Registering voters step is not started.");
        require(
            !voters[voter].hasVoted,
            "The voter already hasVoted."
        );
        require(voters[voter].isRegistered == false, "Already added.");
        voters[voter].isRegistered = true;
        voters[voter].weight = weight;
        numberOfParticipants++;
        emit VoterRegistered(voter);
    }

    /// Give your vote (including votes delegated to you)
    /// to proposal `proposals[proposal].name`.
    function vote(uint proposal) external {
        require(step == WorkflowStatus.VotingSessionStarted, "Voting time is not started.");
        Voter storage sender = voters[msg.sender];
        require(sender.isRegistered, "Has no right to vote.");
        require(!sender.hasVoted, "Already hasVoted.");
        sender.hasVoted = true;
        sender.vote = proposal;
        numberOfVoters++;
        // If `proposal` is out of the range of the array,
        // this will throw automatically and revert all
        // changes.
        proposals[proposal].voteCount += sender.weight;
        emit HasVoted(msg.sender, proposal);
    }

    function registerProposal(string memory proposalName) external {
        require(step == WorkflowStatus.ProposalsRegistrationStarted, "Proposals registration is not started.");
        Voter storage sender = voters[msg.sender];
        require(sender.isRegistered, "Has no right to register proposal.");
        proposals.push(Proposal({
            name: proposalName,
            voteCount: 0
        }));
        emit ProposalRegistered(proposals.length - 1);
    }

    function tallyVotes() public onlyOwner {
        require(step == WorkflowStatus.VotingSessionEnded, "Not time to count votes.");
        step = WorkflowStatus.VotesTallied;
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposalId = p;
            }
        }
    }

    /// @dev Computes the winning proposal taking all
    /// previous votes into account.
    function winningProposal() public view
            returns (uint winningProposal_)
    {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        winningProposal_ = winningProposalId;
    }

    function getNumberOfVotes() public view onlyOwner
            returns (uint numberOfVotes_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        numberOfVotes_ = numberOfVoters;
    }

    function getAbstentionPercentage() public view
            returns (uint abstentionPercentage_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        abstentionPercentage_ = (numberOfParticipants - numberOfVoters) * 100 / numberOfParticipants;
    }

    function getNumberOfParticipants() public view
            returns (uint numberOfParticipants_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        numberOfParticipants_ = numberOfParticipants;
    }

    // Calls winningProposal() function to get the index
    // of the winner contained in the proposals array and then
    // returns the name of the winner
    function getWinner() external view
            returns (string memory winnerName_)
    {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        winnerName_ = proposals[winningProposal()].name;
    }
}