> Nathan Fontaine <nathan.fontaine.etu@univ-lille.fr>  
> BUT 3 Informatique - Groupe M  
> IUT de Lille, site de Villeneuve d'Ascq  
> Octobre 2023  

## Description

Un contrat intelligent élaboré en Solidity, conçu pour fonctionner comme un système de vote.